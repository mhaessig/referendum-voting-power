#[macro_use]
extern crate anyhow;
extern crate globwalk;

use std::fs::File;
use std::io::{BufReader, Read};

use anyhow::{Context, Result};
use csv::{QuoteStyle, Terminator};

use agreement::Agreement;
use partition::UrbanRuralPartition;
use referendum::{MajorityKind, Referendum, ReferendumKind};

mod agreement;
mod calc;
mod municipality_info;
mod output;
mod partition;
mod referendum;

fn main() -> Result<()> {
    let municipality_infos = municipality_info::from_csv("data/raumgliederung.csv")?;

    let mut single_maj_referenda: Vec<Referendum> = Vec::new();
    let mut double_maj_referenda: Vec<Referendum> = Vec::new();
    for json_path in globwalk::glob("data/*.json")? {
        if let Ok(json_path) = json_path {
            eprintln!("Reading {:?}", json_path.file_name());
            let json_file = File::open(json_path.path())?;
            let mut b = BufReader::new(json_file);
            let mut json = String::new();
            b.read_to_string(&mut json)?;

            let mut single = referendum::referendums_from_json(
                &json,
                MajorityKind::SingleMajority,
                vec![
                    ReferendumKind::Initiative,
                    ReferendumKind::Optional,
                    ReferendumKind::Mandatory,
                ],
            )?;
            single_maj_referenda.append(&mut single);

            let mut double = referendum::referendums_from_json(
                &json,
                MajorityKind::DoubleMajority,
                vec![
                    ReferendumKind::Initiative,
                    ReferendumKind::Optional,
                    ReferendumKind::Mandatory,
                ],
            )?;
            double_maj_referenda.append(&mut double);
        }
    }

    let mut double_maj_agreement = Agreement::new();
    for r in double_maj_referenda {
        //eprintln!("Calculating for {}", r.title);
        let groups = match partition::partition::<UrbanRuralPartition>(&r, &municipality_infos) {
            Ok(p) => p,
            Err(_) => continue,
        };
        double_maj_agreement.record_agreement(&groups, &r)?;
    }
    double_maj_agreement.record_percent();
    double_maj_agreement.compute_indices();
    println!(
        "Agreement in referenda with double majority:\n{}",
        double_maj_agreement
    );

    let mut out_writer = csv::WriterBuilder::new()
        .delimiter(b'|')
        .quote_style(QuoteStyle::Never)
        .double_quote(false)
        .terminator(Terminator::CRLF)
        .from_path("out/majority_indices.csv")?;
    let mut single_maj_agreement = Agreement::new();
    for r in single_maj_referenda {
        //eprintln!("Calculating for {}", r.title);
        let partitions = match partition::partition::<UrbanRuralPartition>(&r, &municipality_infos)
        {
            Ok(p) => p,
            Err(_) => continue,
        };

        single_maj_agreement.record_agreement(&partitions, &r)?;
        let indices = calc::compute_maj_indices(&partitions, &r)?;

        output::output_urban_rural(&mut out_writer, r, partitions, indices)?;
    }

    single_maj_agreement.record_percent();
    single_maj_agreement.compute_indices();
    println!("{}", single_maj_agreement);

    out_writer
        .flush()
        .with_context(|| "Failed to write csv file")
}

use std::fs::File;

use anyhow::{Context, Result};
use serde::Serialize;

use crate::calc::GroupIndices;
use crate::partition::{Partitions, UrbanRuralPartition};
use crate::referendum::{MajorityKind, Referendum};

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
struct UrbanRuralOutput {
    date: String,
    title: String,
    accepted: bool,
    majority: String,
    popular_result: String,
    state_result: String,
    urban_accepted: bool,
    urban_result: String,
    urban_index: String,
    rural_accepted: bool,
    rural_result: String,
    rural_index: String,
}

pub fn output_urban_rural(
    wtr: &mut csv::Writer<File>,
    referendum: Referendum,
    groups: Partitions<UrbanRuralPartition>,
    indices: GroupIndices<UrbanRuralPartition>,
) -> Result<()> {
    let urban_group = groups.get(&UrbanRuralPartition::Urban).unwrap();
    let rural_group = groups.get(&UrbanRuralPartition::Rural).unwrap();
    let record = UrbanRuralOutput {
        date: referendum.date.format("%d.%m.%Y").to_string(),
        title: referendum.title.clone(),
        accepted: referendum.accepted,
        majority: referendum.majority.clone().to_string(),
        popular_result: referendum.results.to_string(),
        state_result: if referendum.majority == MajorityKind::SingleMajority {
            String::from("")
        } else {
            referendum.state_results.clone().unwrap().to_string()
        },
        urban_accepted: urban_group.accepted,
        urban_result: urban_group.result.to_string(),
        urban_index: format!("{:.5}", indices.get(&UrbanRuralPartition::Urban).unwrap()),
        rural_accepted: rural_group.accepted,
        rural_result: rural_group.result.to_string(),
        rural_index: format!("{:.5}", indices.get(&UrbanRuralPartition::Rural).unwrap()),
    };

    wtr.serialize(record).with_context(|| {
        format!(
            "Failed to serialize record for referendum with id {}",
            referendum.id
        )
    })
}

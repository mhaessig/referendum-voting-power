use std::collections::HashMap;

use anyhow::Result;
use statrs::distribution::{Normal, Univariate};

use crate::partition::{Partition, Partitions};
use crate::referendum::Referendum;

pub type GroupIndices<P> = HashMap<P, f64>;

fn compute_maj_measure(n1: u64, n2: u64, n: u64, p: f64) -> Result<f64> {
    let (n1_f, n2_f, n_f) = (n1 as f64, n2 as f64, n as f64);
    let rho = n1_f * (1.0 + p * (n1_f - 1.0) + p * n2_f - n_f * p)
        / (n1_f.sqrt() * n_f.sqrt() * (1.0 - p));

    let mean_y = n_f * p;
    let var_y = mean_y * (1.0 - p);

    let mean_yg = n1_f * p;
    let var_yg = mean_yg * (1.0 - p);

    let n1_dist = Normal::new(mean_yg, var_yg)?;

    let half_n_floor = (0.5 * n_f).floor();
    let half_n1_floor = (0.5 * n1_f).floor();

    let mut upper_sum = 0.0;
    for i in (half_n1_floor as u64 + 1)..=n1 {
        let i_f = i as f64;
        let cond_mean = mean_y + rho * var_y.sqrt() * (i_f - mean_yg) / var_yg.sqrt();
        let cond_var = var_y.sqrt() * (1.0 - rho * rho).sqrt();
        let cond_dist = Normal::new(cond_mean, cond_var)?;
        upper_sum += 1.0 - cond_dist.cdf(half_n_floor);
    }
    let upper_conditional = 1.0 - n1_dist.cdf(half_n_floor);
    let p1 = upper_sum * upper_conditional;

    let mut lower_sum = 0.0;
    for i in 0..=(half_n1_floor as u64) {
        let i_f = i as f64;
        let cond_mean = mean_y + rho * var_y.sqrt() * (i_f - mean_yg) / var_yg.sqrt();
        let cond_var = var_y.sqrt() * (1.0 - rho * rho).sqrt();
        let cond_dist = Normal::new(cond_mean, cond_var)?;
        lower_sum += cond_dist.cdf(half_n_floor);
    }
    let lower_conditional = n1_dist.cdf(half_n_floor);
    let p2 = lower_sum * lower_conditional;

    let measure = 2.0 * (p1 + p2) - 1.0;
    Ok(measure)
}

pub fn compute_maj_indices<P>(
    groups: &Partitions<P>,
    referendum: &Referendum,
) -> Result<GroupIndices<P>>
where
    P: Partition,
{
    assert_eq!(groups.len(), 2); // for now we assume we can only have 2 groups
    let n = referendum.results.valid_votes;

    let mut indices: GroupIndices<P> = HashMap::new();
    let mut sum = 0.0;

    let mut iter_keys = groups.keys();
    let mut cats: Vec<&P> = Vec::new();
    cats.push(iter_keys.next().unwrap());
    cats.push(iter_keys.next().unwrap());

    for i in 0..2 {
        let j = if i == 1 { 0 } else { 1 };
        let cat = *cats.get(i).unwrap();
        let other_cat = *cats.get(j).unwrap();
        let measure = compute_maj_measure(
            groups.get(cat).unwrap().result.valid_votes,
            groups.get(other_cat).unwrap().result.valid_votes,
            n,
            0.5,
        )?;

        sum += measure;
        indices.insert(*cat, measure);
    }

    for i in 0..2 {
        let cat = *cats.get(i).unwrap();
        let measure = indices.get_mut(cat).unwrap();
        let index = *measure / sum;
        indices.insert(*cat, index);
    }

    Ok(indices)
}

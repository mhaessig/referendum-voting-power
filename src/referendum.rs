use std::convert::{TryFrom, TryInto};
use std::fmt;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

use anyhow::{Context, Result};
use chrono::prelude::*;
use serde::{de, Deserialize, Deserializer};

/// Represents the two different kinds of majorities in Swiss referenda.
///
/// In the data from the statistics office this is encoded as a bool.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum MajorityKind {
    /// Doppeltes Mehr meaning the popular majority and the weighted majority of states is required.
    DoubleMajority,
    /// Einfaches Mehr meaning only the popular mojority is required.
    SingleMajority,
}

impl From<bool> for MajorityKind {
    fn from(double_majority: bool) -> Self {
        if double_majority {
            MajorityKind::DoubleMajority
        } else {
            MajorityKind::SingleMajority
        }
    }
}

impl Display for MajorityKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::DoubleMajority => write!(f, "double majority"),
            Self::SingleMajority => write!(f, "single majority"),
        }
    }
}

/// Represents the different possible kinds of referenda in Switzerland
///
/// In referendum data from the statistics office this is encoded by integers from 1 through 5.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ReferendumKind {
    /// Obligatorisches Referendum, value 1
    Mandatory,
    /// Fakultatives Referendum, value 2
    Optional,
    /// Volksinitiative ohne direkten Gegenvorschlag, value 3
    Initiative,
    /// Volksinitiative mit direktem Gegenvorschlag, Initiative, value 4
    InitiativeWithCounterproposal,
    /// Volksinitiative mit direktem Gegenvorschlag, Gegenvorschlag, value 5
    CounterproposalToInitiative,
    /// Stichfrage bei direktem Gegenvorschlag, value 6
    AllTypes,
}

impl TryFrom<u64> for ReferendumKind {
    type Error = anyhow::Error;

    fn try_from(value: u64) -> Result<Self, Self::Error> {
        match value {
            1 => Ok(ReferendumKind::Mandatory),
            2 => Ok(ReferendumKind::Optional),
            3 => Ok(ReferendumKind::Initiative),
            4 => Ok(ReferendumKind::InitiativeWithCounterproposal),
            5 => Ok(ReferendumKind::CounterproposalToInitiative),
            6 => Ok(ReferendumKind::AllTypes),
            n => bail!("Unknown referendum variant: {}", n),
        }
    }
}

impl Display for ReferendumKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Mandatory => write!(f, "mandatory referendum"),
            Self::Optional => write!(f, "optional referendum"),
            Self::Initiative => write!(f, "popular initiative"),
            Self::InitiativeWithCounterproposal => {
                write!(f, "popular initiative with counterproposal")
            }
            Self::CounterproposalToInitiative => write!(f, "counterproposal to popular initiative"),
            Self::AllTypes => write!(f, "all types"),
        }
    }
}

/// The referendum struct represents the results of a Swiss referendum on the federal level, the
/// cantonal level and the municipal level.
///
/// It corresponds more or less to the structure of the data from the statistics office.
#[derive(Debug)]
pub struct Referendum {
    pub id: u64,
    pub title: String,
    pub date: Date<Utc>,
    pub kind: ReferendumKind,
    pub majority: MajorityKind,
    pub accepted: bool,
    pub results: Results,
    /// Only present for double majorities
    pub state_results: Option<StateResults>,
    pub cantons: Vec<Canton>,
}

/// This struct represents the generic structure of the actual results (as in votes) for a referendum.
/// It is used on all levels.
#[derive(Clone, Debug, Default, Deserialize)]
pub struct Results {
    //#[serde(rename = "anzahlStimmberechtigte")]
    //pub voting_population: u64,
    #[serde(rename = "gueltigeStimmen")]
    pub valid_votes: u64,
    #[serde(rename = "jaStimmenAbsolut")]
    pub yay_votes: u64,
    #[serde(rename = "neinStimmenAbsolut")]
    pub no_votes: u64,
}

impl Display for Results {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "valid_votes_cast: {}, yes: {} ({}%), no: {} ({}%)",
            // self.voting_population,
            self.valid_votes,
            self.yay_votes,
            self.yay_votes as f64 / self.valid_votes as f64 * 100.0,
            self.no_votes,
            self.no_votes as f64 / self.valid_votes as f64 * 100.0
        )
    }
}

/// The results for a State majority (in case of a double majority)
#[derive(Clone, Debug, Default, Deserialize)]
pub struct StateResults {
    #[serde(rename = "anzahlStaendeGanz")]
    pub states_full: u64,
    #[serde(rename = "jaStaendeGanz")]
    pub states_full_yay: u64,
    #[serde(rename = "neinStaendeGanz")]
    pub states_full_nay: u64,
    #[serde(rename = "anzahlStaendeHalb")]
    pub states_half: u64,
    #[serde(rename = "jaStaendeHalb")]
    pub states_half_yay: u64,
    #[serde(rename = "neinStaendeHalb")]
    pub states_half_nay: u64,
}

impl Display for StateResults {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}/{}",
            self.states_full_yay as f64 + 0.5 * self.states_half_yay as f64,
            self.states_full as f64 + 0.5 * self.states_half as f64
        )
    }
}

#[derive(Debug, Default, Deserialize)]
pub struct Canton {
    #[serde(rename = "geoLevelnummer")]
    #[serde(deserialize_with = "from_str")]
    pub id: u64,
    #[serde(rename = "geoLevelname")]
    pub name: String,
    #[serde(rename = "resultat")]
    pub result: Results,
    #[serde(rename = "gemeinden")]
    pub municipalities: Vec<Municipality>,
}

#[derive(Debug, Default, Deserialize)]
pub struct Municipality {
    /// Corresponds to the BFI id for municipaities
    #[serde(rename = "geoLevelnummer")]
    #[serde(deserialize_with = "from_str")]
    pub id: u64,
    #[serde(rename = "geoLevelname")]
    pub name: String,
    #[serde(rename = "resultat")]
    pub result: Results,
}

pub fn referendums_from_json(
    json: &str,
    ret_majority: MajorityKind,
    ret_kind: Vec<ReferendumKind>,
) -> Result<Vec<Referendum>> {
    // if the json string contains a BOM we need to remove it
    let s: &str;
    if &json[0..3] == "\u{feff}" {
        s = &json[3..];
    } else {
        s = json;
    }

    let j: serde_json::Value =
        serde_json::from_str(s).with_context(|| "Failed to parse json string")?;

    let datestring = j["abstimmtag"]
        .as_str()
        .with_context(|| format!("Field \"abstimmtag\" ({}) is not a string", j["abstimmtag"]))?;
    let date = Utc
        .datetime_from_str(&format!("{}000000", datestring), "%Y%m%d%H%M%S")?
        .date();

    let mut ret_vec: Vec<Referendum> = Vec::new();
    let vorlagen = j["schweiz"]["vorlagen"].as_array().with_context(|| {
        format!(
            "Field \"vorlagen\" ({}) is not an array",
            j["schweiz"]["vorlagen"]
        )
    })?;
    for r in vorlagen {
        let majority: MajorityKind = r["doppeltesMehr"]
            .as_bool()
            .with_context(|| format!("Field {:?} is not a bool", r["doppeltesMehr"]))?
            .into();
        let kind: ReferendumKind = r["vorlagenArtId"]
            .as_u64()
            .with_context(|| "Field \"vorlagenArtId\" is not an integer")?
            .try_into()?;

        eprintln!("majority: {}, kind: {}", majority, kind);
        if majority != ret_majority || !ret_kind.contains(&kind) {
            eprintln!("continuing...");
            continue;
        }

        let cantons= match serde_json::from_value(r["kantone"].clone()) {
            Ok(v) => v,
            Err(_) => continue
        };


        let referendum = Referendum {
            id: r["vorlagenId"]
                .as_u64()
                .with_context(|| "Field \"volagenId\" is not an integer")?,
            title: r["vorlagenTitel"][0]["text"].to_string(),
            date,
            kind,
            majority: majority.clone(),
            accepted: r["vorlageAngenommen"]
                .as_bool()
                .with_context(|| "Field \"vorlageAngenommen\" is not a bool")?,
            results: serde_json::from_value(r["resultat"].clone())?,
            state_results: if majority == MajorityKind::DoubleMajority {
                Some(serde_json::from_value(r["staende"].clone())?)
            } else {
                None
            },
            cantons,
        };

        ret_vec.push(referendum);
    }

    Ok(ret_vec)
}

fn from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: FromStr,
    T::Err: Display,
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    T::from_str(&s).map_err(de::Error::custom)
}

use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};

use anyhow::Result;
use serde::{Deserialize, Deserializer};

/// This enum represents the official languages of Switzerland. For our purposes we use them to
/// classify municipalities as in the [2016 report on language regions of Switzerland](https://www.bfs.admin.ch/bfs/de/home/statistiken/querschnittsthemen/raeumliche-analysen/raeumliche-gliederungen/analyseregionen.assetdetail.2546353.html).
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(remote = "Language")]
pub enum Language {
    German,
    French,
    Italian,
    Rumatsch,
}

impl TryFrom<u64> for Language {
    type Error = anyhow::Error;

    fn try_from(value: u64) -> Result<Self> {
        let lang = match value {
            1 => Language::German,
            2 => Language::French,
            3 => Language::Italian,
            4 => Language::Rumatsch,
            n => bail!("Unknown language code {}", n),
        };

        Ok(lang)
    }
}

impl<'de> Deserialize<'de> for Language {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        u64::deserialize(deserializer)?
            .try_into()
            .map_err(serde::de::Error::custom)
    }
}

/// This enum represents the urban/rural categories for Swiss municipalities as presented in the
/// [2012 report on municipal typoligies](https://www.bfs.admin.ch/bfs/de/home/statistiken/querschnittsthemen/raeumliche-analysen/raeumliche-gliederungen/raeumliche-typologien.assetdetail.2543323.html).
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(remote = "UrbanRuralCategory")]
pub enum UrbanRuralCategory {
    Urban,
    Intermediate,
    Rural,
}

impl TryFrom<u64> for UrbanRuralCategory {
    type Error = anyhow::Error;

    fn try_from(value: u64) -> Result<Self> {
        let cat = match value {
            1 => UrbanRuralCategory::Urban,
            2 => UrbanRuralCategory::Intermediate,
            3 => UrbanRuralCategory::Rural,
            n => bail!("Unknown urban/rural category code {}", n),
        };

        Ok(cat)
    }
}

impl<'de> Deserialize<'de> for UrbanRuralCategory {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        u64::deserialize(deserializer)?
            .try_into()
            .map_err(serde::de::Error::custom)
    }
}

/// This struct represents a subset of the data of the "Raumgliederung" information available from
/// the federal statistics office.
///
/// The data is available as a [REST-API](https://www.bfs.admin.ch/bfs/de/home/dienstleistungen/forschung/api/api-gemeinde.html).
/// The record for this struct can be found [here](https://sms.bfs.admin.ch/WcfBFSSpecificService.svc/AnonymousRest/communes/levels\?startPeriod\=01-01-2021\&endPeriod\=01-01-2021\&useBfsCode\=true\&labelLanguages\=en).
#[derive(Clone, Debug, Deserialize)]
pub struct MunicipalityInfo {
    /// This identifier refers to the BFI id for municipalities
    #[serde(rename = "Identifier")]
    pub id: u64,
    #[serde(rename = "Name_en")]
    pub name: String,
    #[serde(rename = "HR_SPRGEB2016")]
    pub lang: Language,
    #[serde(rename = "HR_GDETYP2012_L1")]
    pub category: UrbanRuralCategory,
}

pub fn from_csv(path: &str) -> Result<HashMap<u64, MunicipalityInfo>> {
    let mut rdr = csv::Reader::from_path(path)?;
    let mut infos: HashMap<u64, MunicipalityInfo> = HashMap::new();

    for result in rdr.deserialize() {
        let record: MunicipalityInfo = result?;
        infos.insert(record.id, record);
    }

    Ok(infos)
}

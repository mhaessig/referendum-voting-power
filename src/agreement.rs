use std::collections::HashMap;
use std::fmt::{Display, Formatter};

use anyhow::Result;

use crate::partition::{Partition, Partitions};
use crate::referendum::Referendum;

pub struct Agreement<P: Partition> {
    no_referenda: u64,
    no_agreements: HashMap<P, u64>,
    percent_agreement: HashMap<P, f64>,
    empirical_indices: HashMap<P, f64>,
}

impl<P: Partition> Agreement<P> {
    pub fn new() -> Self {
        let cats = P::list_categories();

        let mut no_agreements: HashMap<P, u64> = HashMap::new();
        let percent_agreement: HashMap<P, f64> = HashMap::new();
        let empirical_indices: HashMap<P, f64> = HashMap::new();
        for c in cats {
            no_agreements.insert(c, 0);
        }

        Agreement {
            no_referenda: 0,
            no_agreements,
            percent_agreement,
            empirical_indices,
        }
    }

    pub fn record_agreement(&mut self, p: &Partitions<P>, r: &Referendum) -> Result<()> {
        self.no_referenda += 1;

        let cats = P::list_categories();
        for c in cats {
            if p.get(&c).unwrap().accepted == r.accepted {
                let n = self.no_agreements.get(&c).unwrap();
                self.no_agreements.insert(c, n + 1);
            }
        }

        Ok(())
    }

    pub fn record_percent(&mut self) {
        let cats = P::list_categories();
        for c in cats {
            self.percent_agreement.insert(
                c,
                *self.no_agreements.get(&c).unwrap() as f64 / self.no_referenda as f64 * 100.0,
            );
        }
    }

    pub fn compute_indices(&mut self) {
        let mut sum = 0.0;
        let cats = P::list_categories();
        for c in cats.iter() {
            let measure = 2.0 * *self.no_agreements.get(&c).unwrap() as f64 / self.no_referenda as f64 - 1.0;
            self.empirical_indices.insert( *c, measure);

            sum += measure;
        }

        for c in cats.iter() {
            let measure = self.empirical_indices.get(&c).unwrap();
            self.empirical_indices.insert(*c, measure / sum);
        }
    }
}

impl<P: Partition> Display for Agreement<P> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let cats = P::list_categories();
        let mut s = String::new();
        for c in cats {
            s += &format!(
                "{}: agrees {} times out of {} ({:.2}%), empirical power index: {}\n",
                c,
                self.no_agreements.get(&c).unwrap(),
                self.no_referenda,
                self.percent_agreement.get(&c).unwrap(),
                self.empirical_indices.get(&c).unwrap()
            );
        }

        write!(f, "{}", s)
    }
}

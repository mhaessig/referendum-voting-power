use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::hash::Hash;

use anyhow::{Context, Result};

use crate::municipality_info::{Language, MunicipalityInfo, UrbanRuralCategory};
use crate::referendum::{Referendum, Results};

pub type Partitions<P> = HashMap<P, Group<P>>;

pub trait Partition
where
    Self: Clone + Copy + Default + Display + Eq + Hash + Sized,
{
    fn categorize(m: &MunicipalityInfo) -> Self;
    fn make_partitions() -> Partitions<Self>;
    fn list_categories() -> Vec<Self>;
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum UrbanRuralPartition {
    Urban,
    Rural,
}

impl Default for UrbanRuralPartition {
    fn default() -> Self {
        Self::Urban
    }
}

impl Display for UrbanRuralPartition {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Partition for UrbanRuralPartition {
    fn categorize(m: &MunicipalityInfo) -> Self {
        match m.category {
            UrbanRuralCategory::Urban => Self::Urban,
            UrbanRuralCategory::Intermediate => Self::Rural,
            UrbanRuralCategory::Rural => Self::Rural,
        }
    }

    fn make_partitions() -> Partitions<Self> {
        let mut hm: Partitions<Self> = HashMap::new();
        hm.insert(Self::Rural, Group::<Self>::new(Self::Rural));
        hm.insert(Self::Urban, Group::<Self>::new(Self::Urban));

        hm
    }
    
    fn list_categories() -> Vec<Self> {
        vec![Self::Rural, Self::Urban]
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum GermanLatinPartition {
    German,
    Latin,
}

impl Default for GermanLatinPartition {
    fn default() -> Self {
        Self::German
    }
}

impl Display for GermanLatinPartition {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Partition for GermanLatinPartition {
    fn categorize(m: &MunicipalityInfo) -> Self {
        match m.lang {
            Language::German => Self::German,
            _ => Self::Latin,
        }
    }

    fn make_partitions() -> Partitions<Self> {
        let mut hm: Partitions<Self> = HashMap::new();
        hm.insert(Self::German, Group::<Self>::new(Self::German));
        hm.insert(Self::Latin, Group::<Self>::new(Self::Latin));

        hm
    }
    
    fn list_categories() -> Vec<Self> {
        vec![Self::German, Self::Latin]
    }
}

#[derive(Clone, Debug)]
pub struct Group<P>
where
    P: Partition,
{
    pub partition: P,
    pub accepted: bool,
    pub result: Results,
}

impl<P: Partition> Group<P> {
    fn new(partition: P) -> Self {
        Group {
            partition,
            accepted: false,
            result: Results::default(),
        }
    }

    fn add_to_result(&mut self, to_add: &Results) {
        //self.result.voting_population += to_add.voting_population;
        self.result.valid_votes += to_add.valid_votes;
        self.result.yay_votes += to_add.yay_votes;
        self.result.no_votes += to_add.no_votes;
    }
}

pub fn partition<P: Partition>(
    referendum: &Referendum,
    municipality_infos: &HashMap<u64, MunicipalityInfo>,
) -> Result<HashMap<P, Group<P>>> {
    let mut partitions = P::make_partitions();
    for c in &referendum.cantons {
        for m in &c.municipalities {
            let cat: P;
            if m.id < 9000 {
                let info = municipality_infos.get(&m.id).with_context(|| {
                    format!(
                        "Municipality information with id {} and name {} not found",
                        m.id, m.name
                    )
                })?;
                cat = P::categorize(info);
            } else {
                cat = P::default();
            }
            let group = partitions
                .get_mut(&cat)
                .with_context(|| format!("Could not find partition with category {}", &cat))?;

            group.add_to_result(&m.result);
        }
    }

    for (_, group) in partitions.iter_mut() {
        group.accepted = group.result.yay_votes > group.result.no_votes;
    }

    Ok(partitions)
}

# Voting Power in Swiss referenda
This program computes the voting power of groups of individual voters in Swiss
referenda. It is used for my final report in the course "Mathematics in Politics
and Law" in the fall semester 2020.

## Getting started
In order to get started, you need `rust` version 1.49 or later plus `cargo`.

## Data
The data was obtained from the [federal statistical office](https://www.bfs.admin.ch/bfs/de/home/statistiken/kataloge-datenbanken/tabellen.html?dyn_prodima=901283&dyn_institution=900001&dyn_publishingyearend=2021&dyn_title=JSON)
and can be found in the `data` folder. Each file contains a timestamp of when it was downloaded. The data for all
referenda is aggregated to the current cities.